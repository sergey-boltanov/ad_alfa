

#ifndef AD_Tests_AlfaDirectTestSuite_h
#define AD_Tests_AlfaDirectTestSuite_h

#include <CppUnit/TestSuite.h>
#include "ad/tests/Tests.h"

namespace AD
{
	namespace Tests
	{
		class AlfaDirectTestSuite
		{
		public:
			static CppUnit::Test* suite(const std::string& configPath);
		};
	}
}

#endif // AD_Tests_AlfaDirectTestSuite_h
