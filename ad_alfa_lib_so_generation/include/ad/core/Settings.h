﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Core_Settigs_h
#define AD_Core_Settigs_h

#include <ad/core/Core.h>
#include <ad/data/FrontEndType.h>
#include <ad/data/AuthenticationRequestEntity.h>
#include <ad/data/AuthorizeWithConnectionKeyEntity.h>
#include <ad/netio/ConnectionInfo.h>
#include <ad/netio/SocketAddress.h>
#include <map>
#include <vector>

namespace AD
{
	namespace Core
	{
		/*!
			Список IP адресов фронтенда
		*/

		class AD_Core_API AddressList
		{
		public:
			AddressList();

			///добавляет IP адрес в список  
			AddressList& operator << (const NetIO::SocketAddress& address);
			///парсит IP адрес в формате IP:port и добавляет в конец списка
			AddressList& operator << (const String& address);
			///добавить список IP адресов в формате IP:port
			AddressList& operator << (const std::vector<String>& addressList);

			///получить текущий IP. При каждом вызове возращает следующий по списку 
			const NetIO::SocketAddress& get() const;

			String toString(const String& format = String()) const;

			std::vector<NetIO::SocketAddress> address;
			mutable int current;
		};

		typedef std::map<Data::FrontEndType, AddressList> FrontEndAddressMap;

		/*!
			Класс с настройками подключения
		*/
		class AD_Core_API ConnectionSettings
		{
		public:
			ConnectionSettings();
			String toString(const String& format = String()) const;
			
			NetIO::Protocol protocol;				///< протокол(по умолчанию TCP)
			bool isSecureSocketsAvailable;			///< true - резрешено использовать SSL(по умолчанию true)
			NetIO::SocketAddress proxy;				///< настройки прокси
			FrontEndAddressMap frontEndSettings;	///< IP адреса фронтендов
		};

		/*!
			Класс агрегирующий настройки AlfaDirect API
		*/

		class AD_Core_API Settings
		{
			friend class FrontEnds;

		public:	
			/*!
				Инициализировать объект с настройками
				\param[in] код разработчика
				\param[in] clientSoftwareVersion версия клиента
			*/
			Settings(const String& developerCode, const String& clientSoftwareVersion);
			Settings& setConnectionSettings(const ConnectionSettings& connectionSettings);

			String toString(const String& format = String()) const;
			
			String developerCode;									///< Код разработчика
			String deviceModel;										///< Наименование модели устройства
			String osVersion;										///< Версия операционной системы
			String clientSoftwareVersion;							///< Версия клиента
			Data::ClientType clientType;							///< Тип терминала. По умолчанию Data::ClientType::ExternalRobot
			String idDevice;										///< Уникальный иденификатор устройства
			String filesDir;										///< Директория для хранения закэшированных данных. По умолчанию "./cache"
			String homeDir;											///< Каталог в котором установлено приложение
			String userName;										///< Имя учетной записи на устройстве
			ConnectionSettings connectionSettings;					///< Настройки подключения к фронтендам

			String mac() const;
			String getFilesDir() const;

		private:
			Data::AuthenticationRequestEntity buildAuthenticationRequest(const String& login, const String& password) const;
			Data::AuthorizeWithConnectionKeyEntity buildInitFrontendRequest(const String& login, Int64 connectionKey) const;
			NetIO::ConnectionInfo connectionInfo(const Data::FrontEndType& id) const;
		};
	}
}

#endif