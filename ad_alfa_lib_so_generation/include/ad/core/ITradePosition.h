﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Core_ITradePosition_h
#define AD_Core_ITradePosition_h

#include <ad/core/Core.h>
#include <ad/core/IField.h>
#include <ad/core/SymbolSpec.h>
#include <ad/system/AdvancedEnum.h>
#include <ad/data/RazdelGroupType.h>

namespace AD
{
	namespace Core
	{
		/*!
			\brief Интерфейс получения данных по позиции
			

			|	FieldID				|	Title			|				Dependencies																																																																|
			|-----------------------|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
			|	SubAccount			|	Субсчёт			|	Data::SubAccountRazdelEntity::codeSubAccount																																																											|
			|	NameBalanceGroup	|	Портфель		|	Data::SubAccountRazdelEntity::idRazdelGroup																																																												|
			|	SymbolObject		|	Тикер			|	Data::ObjectEntity::SymbolObject																																																														|
			|	NameObject			|	Выпуск			|	Data::ObjectEntity::nameObject																																																															|
			|	BackPos				|	Вх. Позиция		|	ClientSubAccPositionEntity::backPos + ClientSubAccPositionEntity::buyQty - ClientSubAccPositionEntity::sellQty - ClientSubAccPositionEntity::sessionBuyQty + ClientSubAccPositionEntity::sessionSellQty																					|
			|	PrevQuote			|	Вх. Цена		|	IQuote::PrevQuote																																																																		|
			|	BackPosRur			|	Вх. Стоимость	|	BackPos * (PrevQuote * (ISymbol::PriceStepCost / ISymbol::PriceStep) * ClientSubAccPositionEntity::curRate)																																												|
			|	TorgPos				|	Позиция			|	FORTS:	BackPos + TurnIn - TurnOut <BR>	SELT:	BackPos + TurnIn - TurnOut + ClientSubAccPositionEntity::buyQty - ClientSubAccPositionEntity::sellQty <BR> MICEX:	BackPos + TurnIn - TurnOut + ClientSubAccPositionEntity::sessionBuyQty - ClientSubAccPositionEntity::sessionSellQty	|
			|	SubAccNalPos		|	Тек. Позиция	|	ClientSubAccPositionEntity::nalPos																																																														|
			|	Price				|	Цена			|	IQuote::Last																																																																			|
			|	TorgPosRur			|	Стоимость		|	TorgPos * (Price * (ISymbol::PriceStepCost / ISymbol::PriceStep) * ClientSubAccPositionEntity::curRate)																																													|
			|	UchPrice			|	Уч. Цена		|	ClientSubAccPositionEntity::uchPrice																																																													|
			|	TorgPosUch			|	Уч. Стоимость	|	TorgPos * UchPrice																																																																		|
			|	DailyBuyQuantity	|	Куплено, шт		|	ClientSubAccPositionEntity::sessionBuyQty																																																												|
			|	DailyBuyVolume		|	Куплено, руб	|	ClientSubAccPositionEntity::sessionBuyVal																																																												|
			|	DailySellQuantity	|	Продано, шт		|	ClientSubAccPositionEntity::sessionSellQty																																																												|
			|	DailySellVolume		|	Продано, руб	|	ClientSubAccPositionEntity::sessionSellVal																																																												|
			|	TurnIn				|	Вводы			|	ClientSubAccPositionEntity::trnIn																																																														|
			|	TurnOut				|	Выводы			|	ClientSubAccPositionEntity::trnOut																																																														|
			|	DailyPL				|	ПУ(дн), руб		|	TorgPosRur - BackPosRur + DailySellVolume - DailyBuyVolume - (TurnIn - TurnOut) * (PrevQuote * (ISymbol::PriceStepCost / ISymbol::PriceStep) *	ClientSubAccPositionEntity::curRate)																									|
			|	NPL					|	НПУ, руб		|	TorgPosRur - TorgPosUch																																																																	|
			|	NPLPercent			|	НПУ(%)			|	NPL / ABS(TorgPosUch) *100																																																																|
			|	Quantity			|	Кол-во			|	Количество, необходимое для закрытия позиции, кратное размеру лота																																																						|
			|	RazdelCode			|	Раздел			|	Код раздела (ALFA - раздел Альфа деньги)																																																												|
		*/		

		class ITradePosition
		{
		public:
			AD_DECLARE_ENUM(AD_Core_API, FieldID, int,
				ID,
				SubAccount,
				NameBalanceGroup,
				SymbolObject,
				NameObject,
				
				BackPos,
				BackPosRur,
				
				TorgPos,
				TorgPosRur,
				TorgPosUch,
				
				PrevQuote,
				Price,
				
				SubAccNalPos,
				UchPrice,
				
				DailyBuyQuantity,
				DailyBuyVolume,
				
				DailySellQuantity,
				DailySellVolume,

				TurnIn,
				TurnOut,
				
				DailyPL,
				NPL,
				NPLPercent,

				Quantity,
				RazdelCode
			);
			
			virtual Int32 id() const = 0;

			virtual const SymbolSpec& symbol() const = 0;

			virtual const Data::RazdelGroupType& group() const = 0;

			virtual const IField& operator[](const FieldID& field) const = 0;

		protected:
			virtual ~ITradePosition(){}
		};

		class ITradePositionsContainer
		{
		public:

 			/// true, если инит загружен
 			virtual bool isInited() const = 0;

			/// Колличество записей
			virtual Int64 count() const = 0;

			/// Получить данные по аккаунту
			virtual const ITradePosition& get(Int64 index) const = 0;

		protected:
			virtual ~ITradePositionsContainer(){}
		};

	}
}

#endif
