﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Core_IQuoteSourceObserver_h
#define AD_Core_IQuoteSourceObserver_h

#include <ad/core/SourceStatus.h>
#include <ad/system/AdvancedEnum.h>

namespace AD
{
	namespace Core
	{
		class QuoteSpec;
		class ISymbol;
		class IQuote;
		
		AD_DECLARE_ENUM(AD_Core_API, QuoteSourceStatusType, int,
			Created,
			Pending,
			Requesting,
			Failed,
			Ready);

		typedef SourceStatus<QuoteSourceStatusType, QuoteError> QuoteSourceStatus;

		/*!
			\brief Интерфейс наблюдателя данных по котировке
		*/

		class IQuoteSourceObserver
		{
		public:
			/// \brief Вызывается в случае получение данных по инструменту
			/// \param[in] spec параметры запроса клиента
			/// \param[in] symbol параметры торгового инструмента
			virtual void onSymbolResolve(const QuoteSpec& spec, const ISymbol& symbol) = 0;
			
			/// \brief Вызывается в случае обновления данных
			/// \param[in] spec параметры запроса клиента
			/// \param[in] quote котировка
			virtual void onUpdate(const QuoteSpec& spec, const IQuote& quote) = 0;

			/// \brief Вызывается в случае изменения состояния
			/// \param[in] spec параметры запроса клиента
			/// \param[in] status текущее состояние
			/*! 
				|	StatusType	|			Description																			|
				|---------------|-----------------------------------------------------------------------------------------------|
				|	Created		|	Начальное состояние																			|
				|	Pending		|	Ожидание данных по инструменту, справочников, подключения к фронтендам						|
				|	Requesting	|	Получены параметры инструмента. Отправлен запрос на получение котировки						|
				|	Failed		|	Запрос не может быть выполнен. QuoteSourceStatus::error будет содержать данные об ошибке 	|
				|	Ready		|	Начали поступать даные по котировке															|
			*/
			virtual void onUpdateStatus(const QuoteSpec& spec, const QuoteSourceStatus& status) = 0;

		protected:
			virtual ~IQuoteSourceObserver(){}
		};
	}
}

#endif
