﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Codec_SerializationHelper_h
#define AD_Codec_SerializationHelper_h

#include <ad/system/AdvancedEnumBase.h>
#include <ad/system/IsBaseOf.h>
#include <ad/system/IsPrimitive.h>
#include <ad/system/Errors.h>


namespace AD
{
	namespace Codec
	{
		enum ClassType
		{
			Serializable,
			Atomic,
			Enum,
			Flag,
		};

		template <class T>
		struct TypeTraits
		{
			static const ClassType value = System::IsBaseOf<System::AdvancedEnumBase, T>::value ? Enum : 
					System::IsBaseOf<System::EnumBase, T>::value ? Flag :
						System::IsPrimitive<T>::value ? Atomic : Serializable;
			/*static const ClassType value = IsBaseOf<ISerializable, T>::value ? Serializable 
				: IsBaseOf<AdvancedEnumBase, T>::value ? Enum : Atomic;*/
		};
		
		//@{
		/// \br Служит для сериализации унаследованных от ISerializable типов
		/// \be Serves for serialization ISerializable derived types
		template <class T, ClassType type = TypeTraits<T>::value>
		struct SerializationHelper;

		template <class T>
		struct SerializationHelper<T, Atomic>
		{
			template <class BinaryReader>
			static void read(T& data, BinaryReader& reader)
			{
				reader.read((char*)&data, sizeof(T));
			}

			template <class BinaryWriter>
			static void write(T data, BinaryWriter& writer)
			{
				writer.write((const char*)&data, sizeof(data));
			}
		};

		template <class T>
		struct SerializationHelper<T, Serializable>
		{
			template <class BinaryReader>
			static void read(T& data, BinaryReader& reader)
			{
				data.deserialize(reader);
			}

			template <class BinaryWriter>
			static void write(const T& data, BinaryWriter& writer)
			{
				data.serialize(writer);
			}
		};

		template <class T>
		struct SerializationHelper<T, Enum>
		{
			template <class BinaryReader>
			static void read(T& data, BinaryReader& reader)
			{
				typename T::Code code = 0;
				reader.read(code);
				data = T::fromCode(code);
				if (data.isUnknown() && code != 0)
				{
					throw AD_ERROR(UnexpectedCode, "Unexpected {0} code = {1}", typeid(T).name(), Int64(code));
				}
			}

			template <class BinaryWriter>
			static void write(T data, BinaryWriter& writer)
			{
				writer.write(data.toCode());
			}
		};

		template <class T>
		struct SerializationHelper<T, Flag>
		{
			template <class BinaryReader>
			static void read(T& data, BinaryReader& reader)
			{
				reader.read(static_cast<typename T::Type&>(data));
				if (data != T::Unknown && (data < T::Begin || T::End <= data))
				{
					throw AD_ERROR(UnexpectedValue, "Unexpected {0} value = {1}", typeid(T).name(), Int64(data));
				}
			}

			template <class BinaryWriter>
			static void write(T data, BinaryWriter& writer)
			{
				writer.write(static_cast<typename T::Type>(data));
			}
		};
		//@}


		/*!
		\struct SerializationHelper SerializationHelper.h <ad/codec/SerializationHelper.h>
		*/
	}
}

#endif