﻿// Серверное API системы «Альфа-Директ»  © АО «АЛЬФА-БАНК»
//
//
// Permission is hereby granted, free of charge, to any person or organization
// obtaining a copy of the software and accompanying documentation covered by
// this license (the "Software") to use, reproduce, display, distribute,
// execute, and transmit the Software, and to prepare derivative works of the
// Software, and to permit third-parties to whom the Software is furnished to
// do so, all subject to the following:
// 
// The copyright notices in the Software and this entire statement, including
// the above license grant, this restriction and the following disclaimer,
// must be included in all copies of the Software, in whole or in part, and
// all derivative works of the Software, unless such copies or derivative
// works are solely in the form of machine-executable object code generated by
// a source language processor.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
// SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
// FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

#ifndef AD_Data_FinInfoEntity_h
#define AD_Data_FinInfoEntity_h

#include <ad/data/RealtimeEntity.h>
#include <ad/data/TradePeriodType.h>
#include <ad/data/Data.h>

#include <ad/codec/IEntity.h>


namespace AD
{
	namespace Data
	{
		/// \br Представляет квоту
		struct AD_Data_API FinInfoEntity
			: public RealtimeEntity
			, virtual public Codec::IEntity
		{
			/// \br Идентификатор сессии по которой транслируется котировки
			Int32 idSession;
			/// \br Статус торгового периода
			TradePeriodType idTradePeriodStatus;
			/// \br Цена последней сделки или последнее значение индекса
			double last;
			/// \br Объем последней сделки в лотах
			Int32 qty;
			/// \br Биржевое время и дата последней сделки или время последнего расчете индекса
			Timestamp lastTime;
			/// \br Официальная текущая (средневзвешенная) цена
			double quote;
			/// \br Биржевое время изменения поля quote
			Timestamp qtime;
			/// \br Спрос
			double bid;
			/// \br Предложение
			double ask;
			/// \br Доходность или волатильность опциона по цене последней сделки
			double yield;
			/// \br Длителность текущей сессии
			double duration;
			/// \br Объем заявок на покупку по лучшей котировке в лотах
			Int32 bidQty;
			/// \br Объем всех заявок на покупку в очереди торговой системы в лотах
			Int32 sumBid;
			/// \br Объем заявок на продажу по лучшей котировке в лотах
			Int32 askQty;
			/// \br Объем всех заявок на продажу в очереди торговой системы в лотах
			Int32 sumAsk;
			/// \br Заявок на покупку
			Int32 numBids;
			/// \br Заявок на продажу
			Int32 numAsks;
			/// \br Максимальная цена сделок или индекса за текущую сессию
			double high;
			/// \br Минимальная цена сделок или индекса за текущую сессию
			double low;
			/// \br Средневзвешенная цена сделок за текущую сессию
			double waPrice;
			/// \br Доходность по средневзвешенной цене
			double yieldWAPrice;
			/// \br Количество сделок за торговый день
			Int64 numTrades;
			/// \br Объем совершенных сделок, выраженный в единицах ценных бумаг
			Int64 volToday;
			/// \br Объем сделок за текущую сессию или объем сделок по бумагам, входящим в индекс, в рублях
			double valToday;
			/// \br Объем открытых позиций после совершения последней сделки
			Int32 pos;
			/// \br Наибольшая цена спроса в течение текущей торговой сессии
			double highBid;
			/// \br Наименьшая цена предложения в течение текущей торговой сессии
			double lowOffer;
			/// \br Дата и время изменения обновления записи по таймеру биржи
			Timestamp exTime;
			/// \br Min Short Price Micex
			double minCurLast;
			/// \br Цена первой сделки/ Значение индекса на открытие текущего торгового дня
			double open;
			double prevCloseYield;
			double prevPos;
			double prevWAPrice;

			AD_DECLARE_VERSION_ENTITY_STUF(FinInfoEntity);

			void clear();
		};
 

		/*!
		\response{FinInfoEntity}
		\ingroup quote
		\frontends RealTimeBirzInfoServer
		\requests DataFlowSubscribeEntity
		*/
	}
}

#endif
