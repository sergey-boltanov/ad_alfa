#define _GLIBCXX_USE_CXX11_ABI 0

#include <iostream>
#include <ad/core/ITradeComplex.h>
#include <ad/core/IDataSources.h>
#include <ad/core/ISymbol.h>
#include <ad/core/IQuote.h>
#include <ad/core/QuoteSpec.h>
using namespace std;
//реализация интерфейса наблюдателя котировки
class QuoteObserver: public AD::Core::IQuoteSourceObserver{
public:
    virtual void onSymbolResolve(const AD::Core::QuoteSpec& spec, const AD::Core::ISymbol& symbol){
        cout << "symbol resolved: " << spec.symbolSpec().symbol() << ": " << symbol[AD::Core::ISymbol::FieldID::FullName].asString() << endl;
    }
    virtual void onUpdate(const AD::Core::QuoteSpec& spec, const AD::Core::IQuote& quote){
        cout << spec.symbolSpec().symbol() << " ask: " << quote[AD::Core::IQuote::FieldID::Ask].asDouble() << endl;
    }
    virtual void onUpdateStatus(const AD::Core::QuoteSpec& spec, const AD::Core::QuoteSourceStatus& status){
        cout << spec.symbolSpec().symbol() << " status: " << status.toString() << endl;
    }
};
QuoteObserver quoteobserver;
int main(int argc, char** argv){
    //Указываем код разработчика и версию клиента
    AD::Core::Settings settings("201594_892649B5-7694-4925-9EAC-1F719678054B", "0.0.1");
    //задаем настройки подключения к фронтендам
//    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::AuthAndOperInitServer]			<< "172.21.1.63:448";
 //   settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::OperServer]				<< "172.21.1.67:448";
 //   settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::RealTimeBirzInfoServer]		<< "172.21.1.71:448";
 //   settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::BirzArchAndMediaServer]		<< "172.21.1.65:448";
 //   settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::RealTimeBirzInfoDelayedServer] << "172.21.1.69:448";

    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::AuthAndOperInitServer]         << "217.12.99.21:448";
    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::OperServer]                    << "217.12.99.22:448";
    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::RealTimeBirzInfoServer]        << "217.12.99.23:448";
    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::BirzArchAndMediaServer]        << "217.12.99.24:448";
    settings.connectionSettings.frontEndSettings[AD::Data::FrontEndType::RealTimeBirzInfoDelayedServer] << "217.12.99.25:448";
    //инициализируем класс, который является основной точкой взимодействия с Альфа-Директ API
    AD::Core::TradeComplexPtr complex = AD::Core::ITradeComplex::create(settings);
    //задаем параметры инструмента(акции Сбербанка)
    const AD::Core::QuoteSpec quoteSpec(AD::Core::SymbolSpec("SBER", L"МБ ЦК"), false);
    //подписываемся на получение данных по котировке
    const AD::SubscriptionPtr quoteSubscription(complex->datasources().subscribe(quoteSpec, &quoteobserver));
    //авторизация и подключение к фронтендам
    complex->login("ttest96", "6k8vo0");
    for(;;);
    return 0;
}
